---
title: Home
---

# Haskell is easy - a list of recommended libraries

Go to section:

* [HTTP Clients](#http-clients)
* [HTTP Frameworks](#http-frameworks)
* [Note on streaming](#http-frameworks)

To make it more true that Haskell is easy, here's a list of curated libraries. In the personality descriptions matching you to a library, be honest with yourself about where you're at and what your needs are.

## HTTP clients

### I just got here and don't know what I'm doing

* [Wreq](https://hackage.haskell.org/package/wreq)
    - Tutorials and examples:
        * [Wreq tutorial](http://www.serpentine.com/wreq/)
        * [Keeping it easy while querying Github's API](http://bitemyapp.com/posts/2016-02-06-haskell-is-not-trivial-not-unfair.html)

### Know what I'm doing and I'd like more fine-grained control over resources, such as streaming

Please [see the note on streaming](#note-on-streaming) before using any streaming libraries.

- [pipes-http](https://hackage.haskell.org/package/pipes-http)

- [http-conduit](https://hackage.haskell.org/package/http-conduit)


## HTTP Frameworks

### I just need to make a tiny API or I just started

- [Scotty](hackage.haskell.org/package/scotty)
    * [Literate URL shortener](http://bitemyapp.com/posts/2014-11-22-literate-url-shortener.html)
    * [Compact URL shortener](http://bitemyapp.com/posts/2014-08-22-url-shortener-in-haskell.html)

### I'm going to make a web application I have to maintain and I'm comfortable with a more opinionated framework

- [Yesod](http://www.yesodweb.com/)


## Note on streaming

Pipes is generally easier to use than Conduit and can be quite pleasant. However, if you're new to Haskell you should avoid using streaming libraries unless necessary to control memory usage. Mostly because you'll spin your wheels on which operator to use and the type errors won't help much. YMMV. Please don't email me to brag about how you're a genius-wizard that had zero trouble using streaming libraries unless you're going to link a resource or explain a method that made it easy. And if the link is to the standard tutorial for the library, I will not respect you.
