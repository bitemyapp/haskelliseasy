---
title: Home
---

# Haskell is easy - a list of recommended Haskell libraries

To make it more true that Haskell is easy, here's a list of curated libraries. In the personality descriptions matching you to a library, be honest with yourself about where you're at and what your needs are.

No junk, this isn't listing for listing's sake. I've either used it or someone whose taste and experience I _really_ trust has. If you list everything, the list is useless to somebody that can't sniff test the API quickly.

## Getting Haskell installed

- [Stack](http://haskellstack.org)

## Basic data structures

- [containers](http://hackage.haskell.org/package/containers)

## Benchmarking

- [criterion](http://hackage.haskell.org/package/criterion) uses [statistics](#statistics) to help prevent you lying to yourself.

## Binary serialization/deserialization

### Stuff that is somewhat opinionated about how your data looks over the wire

- [cereal](http://hackage.haskell.org/package/cereal)

### I wanna write my own!

- Use the plain old [bytestring](http://hackage.haskell.org/package/bytestring) library and the builder API.

## CSV

### I'm just ginning something up real quick
- [cassava](https://hackage.haskell.org/package/cassava)
    * [HowIStart Haskell tutorial using Cassava](http://howistart.org/posts/haskell/1)

### This is something I need to maintain and I need a more-correct CSV parser

- [csv-conduit](http://hackage.haskell.org/package/csv-conduit) streaming, similar row-oriented deserialization/serialization API.

## Elasticsearch

- [Bloodhound](http://hackage.haskell.org/package/bloodhound)

## HTTP clients

### I just got here and don't know what I'm doing

* [Wreq](https://hackage.haskell.org/package/wreq)
    - Tutorials and examples:
        * [Wreq tutorial](http://www.serpentine.com/wreq/)
        * [Keeping it easy while querying Github's API](http://bitemyapp.com/posts/2016-02-06-haskell-is-not-trivial-not-unfair.html)

### Know what I'm doing and I'd like more fine-grained control over resources, such as streaming or the HTTP connection pool manager

- [http-conduit](https://hackage.haskell.org/package/http-conduit)


## HTTP Frameworks

- [Yesod](http://www.yesodweb.com/)
    * [Yesod book](http://www.yesodweb.com/book)
    * [Carnival](https://github.com/thoughtbot/carnival) open source comments app.
    * [Snowdrift.coop](https://github.com/snowdriftcoop/snowdrift) a non-profit, cooperative platform for funding public goods, specifically Free/Libre/Open (FLO) works.

## JSON

- [aeson](http://hackage.haskell.org/package/aeson)

## lenses

- [lens](http://hackage.haskell.org/package/lens) you'll have to cargo cult early on and it's best if you're not a beginner, but there's nothing better if it's something you're going to use instead of framing the source code and mounting it over your fireplace.

## Logging

- [katip](http://hackage.haskell.org/package/katip) offers plain text based logging to stdout/stderr as well as structured logging to backends like elasticsearch via bindings like: [katip-elasticsearch](http://hackage.haskell.org/package/katip-elasticsearch).

## NLP

- [chatter](http://hackage.haskell.org/package/chatter) a collection of simple Natural Language Processing algorithms.

## Parsing

### I need to parse bytes

- [attoparsec](http://hackage.haskell.org/package/attoparsec)

### I need to deserialize bytes in a predictable layout and don't really need a full blown parser

See, "binary serialization/deserialization" above, but basically:

- [cereal](http://hackage.haskell.org/package/cereal)

### I need to parse text

- [parsers](http://hackage.haskell.org/package/parsers) with [trifecta](http://hackage.haskell.org/package/trifecta) or [attoparsec](http://hackage.haskell.org/package/attoparsec) as the backend.
    * Covered in [Haskell book](http://haskellbook.com/) in the chapter on parsers.

Avoid using a different parsing backend than `trifecta` or `attoparsec` with `parsers`, variation in the backtracking behavior can break your parser. The use of `parsers` and `trifecta` is covered in the [Haskell Book](http://haskellbook.com).

## Regular expressions

- [regex-tdfa](http://hackage.haskell.org/package/regex-tdfa)

## SQL

### I'm new and just want to throw a query over the wire!

Even if you're going to use raw SQL queries with Persistent, you're better off starting with that as a library and writing some models to describe your data.

So, use Persistent and one the following:


#### PostgreSQL

- [persistent-postgresql](http://hackage.haskell.org/package/persistent-postgresql)

#### MySQL

- [persistent-mysql](hackage.haskell.org/package/persistent-mysql)

#### SQLite

- [persistent-sqlite](http://hackage.haskell.org/package/persistent-sqlite)

### I'm not totally new to Haskell and willing to invest in something more type-safe!

- [Persistent](hackage.haskell.org/package/persistent) combined with [Esqueleto](hackage.haskell.org/package/esqueleto) and the database-specific Persistent library as appropriate.
    * [Yesod book chapter on Persistent](http://www.yesodweb.com/book/persistent)
    * For Esqueleto? You're going to trip up a bit early on, especially with the join order. Try to accumulate example code you can work from.


## Statistics

- [statistics](http://hackage.haskell.org/package/statistics)

## Testing

### Unit/spec testing

- [HSpec](http://hspec.github.io/)
    * Covered in [Haskell Book](http://haskellbook.com)'s testing chapter.

### Property testing

- [QuickCheck](http://hackage.haskell.org/package/QuickCheck)
    * [If you find yourself commonly checking typeclass laws](http://hackage.haskell.org/package/checkers)
    * Also covered in [Haskell Book](http://haskellbook.com)'s testing chapter and used throughout the chapters on monoid, semigroup, functor, applicative, and monad.

## Utility libraries and conveniences

- [classy-prelude](http://hackage.haskell.org/package/classy-prelude) can save you some grief, particularly around mixing IO, String, ByteString, and Text. Best used with a [project template](https://github.com/commercialhaskell/stack-templates/blob/master/yesod-hello-world.hsfiles) that disables Prelude by default and gets an import module in place for you.

- [composition-extra](http://hackage.haskell.org/package/composition-extra) mostly for this [bad boy](http://hackage.haskell.org/package/composition-extra-2.0.0/docs/Data-Functor-Syntax.html#v:-60--36--36--62-).

## XML (or HTML, really)

- [xml-conduit](https://hackage.haskell.org/package/xml-conduit) the usual caveats around streaming apply.
